# guitarraNotas

*Si prefieres un resumen en español, lee el fichero*
[`README.es_ES.md`](README.es_ES.md)
*en lugar de este fichero.*

<hr> 

<img src="./img/guitarranotas.png" style="width:40%;">

## What is it?

`guitarraNotas` is a simple application for training musical note
reading on the guitar. It is implemented with HTML, CSS, and
JavaScript. 

It is intended to be used in a classroom setting, and so developed 
with children in mind. In this sense, some apparently arbitrary decisions
 are deliberate. For instance, users cannot choose the duration of a 
round. It is hard-coded according to what I estimate as the best duration 
for children, no less no more. 

## How to use it 

If you just want to play the trainer go to the GitLab page associated
with this project:

<https://lsanjuan.gitlab.io/guitarranotas>

You can also clone or download the whole repository and open `index.html`
in any modern web browser (Chrome and Firefox are actually tested).

## *Note to developers*

The code in the current version is in pre-alpha stage. The trainer is
playable, I hope, but it requires many improvements (proper testing,
cleaning, modularization, ...). We need this educational tool now 
in our institution and hence it is released as soon as possible is spite 
of its rudeness.

This note will be removed when and if I find the time to improve things
everywhere.

