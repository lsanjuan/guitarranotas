# guitarraNotas

<img src="./img/guitarranotas.png" style="width:40%;">

## ¿Qué es guitarraNotas?

`guitarraNotas` es una aplicación sencilla para el entrenamiento de la 
lectura de notas musicales en la guitarra. Está implementado con HTML,
CSS y JavaScript.

La intención es que sea utilizado en el aula. En este sentido, algunas
decisiones aparentemente arbitrarias son deliberadas. Por ejemplo, el
usuario no puede elegir la duración del juego. Está establecida de
antemano de acuerdo con lo que considero la mejor duración para un niño,
ni mayor ni menor.

## ¿Cómo usarlo?

Si sólo quieres ejecutar el entrenador visita la página de GitLab asociada
a este proyecto:

<https://lsanjuan.gitlab.io/guitarranotas>

También puedes descargar o clonar el directorio y abrir `index.html` 
con cualquier navegador moderno.

## *Nota para programadores*

En la actual versión, el código está en una fase completamente inicial.
La aplicación funciona, espero, pero el código requiere muchas mejoras
(tests, refactorización, modularización, ...). Necesitamos ya esta
herramienta en nuestra institución y de ahí que haya sido lanzada
a pesar de ello.

Eliminaré esta nota cuando encuentre el tiempo para mejorar el código.

